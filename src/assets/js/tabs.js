
    
    

	// store tabs variable
   /*  console.log("Test")
    const myTabs = document.querySelectorAll("ul.nav-tabsHP > li"); */
  
   /*  function myTabClicks(tabClickEvent) {
		for (let i = 0; i < myTabs.length; i++) {
			myTabs[i].classList.remove("activatedTab");
		}
		const clickedTab = tabClickEvent.currentTarget;
		clickedTab.classList.add("activatedTab");
		tabClickEvent.preventDefault();
		const myContentPanes = document.querySelectorAll(".tab-paneHP");
		for (i = 0; i < myContentPanes.length; i++) {
			myContentPanes[i].classList.remove("activatedTab");
		}
		const anchorReference = tabClickEvent.target;
		const activePaneId = anchorReference.getAttribute("href");
		const activePane = document.querySelector(activePaneId);
		activePane.classList.add("activatedTab");
	}
	for (i = 0; i < myTabs.length; i++) {
		myTabs[i].addEventListener("click", myTabClicks)
	} */

    const tabFunction = () => {

        const mensTab = document.querySelector('#mensTab');
        const womensTab = document.querySelector('#womensTab');
        const womensTabSlider = document.querySelector('#tab-2');
    
        const tab1 = document.querySelector('#tab-1');
        const tab2 = document.querySelector('#tab-2');
    
        mensTab.addEventListener('click', () => {
            tab2.style.display = "none";
            tab1.style.display = "block";
            mensTab.classList.add('borderBottomTab');
            womensTab.classList.remove('borderBottomTab');
        })
    
        womensTab.addEventListener('click', () => {
            womensTabSlider.classList.remove('visibilityNone')
           tab1.style.display = "none";
            tab2.style.display = "block";
            womensTab.classList.add('borderBottomTab');
            mensTab.classList.remove('borderBottomTab'); 
        })



        const mensTab2 = document.querySelector('#mensTab2');
        const womensTab2 = document.querySelector('#womensTab2');
        const womensTabSlider2 = document.querySelector('#tab2-2');
    
        const tab2_1 = document.querySelector('#tab2-1');
        const tab2_2 = document.querySelector('#tab2-2');
    
        mensTab2.addEventListener('click', () => {
            tab2_2.style.display = "none";
            tab2_1.style.display = "block";
            mensTab2.classList.add('borderBottomTab');
            womensTab2.classList.remove('borderBottomTab');
        })
    
        womensTab2.addEventListener('click', () => {
            womensTabSlider2.classList.remove('visibilityNone')
            tab2_1.style.display = "none";
            tab2_2.style.display = "block";
            womensTab2.classList.add('borderBottomTab');
            mensTab2.classList.remove('borderBottomTab'); 
        })
    
    };
    
    export default tabFunction;
    